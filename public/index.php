<?php

define('APP_DIR', __DIR__ . '/../app');

require_once APP_DIR . '/CarInterface.php';
require_once APP_DIR . '/BmwAbstract.php';
require_once APP_DIR . '/XthreeClass.php';
require_once APP_DIR . '/XfiveClass.php';

$car1 = new XthreeClass('Oleg');

$car1->changeSpeed(0);
$car1->turnOn();
$car1->changeSpeed(1);
$car1->move();
$car1->changeSpeed(2);
$car1->changeSpeed(8);
$car1->changeSpeed(4);
$car1->turnOff();

$car1->showFullInfo();
echo 'Max Speed: ' . XthreeClass::MAX_SPEED . '<br>';



$car2 = new XthreeClass('Roma');
$car2->showFullInfo();
echo 'Max Speed: ' . XthreeClass::MAX_SPEED . '<br>';





