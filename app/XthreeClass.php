<?php

class XthreeClass extends BmwAbstract implements CarInterface
{
    const MAX_SPEED = 5;

    public function __construct($owner)
    {
        $this->owner = $owner;
        $this->price = 10000;
        $this->doorCount = 5;
        $this->engine = 1.2;
        echo "I've bought a car X3!" . '<br>';
    }


    public function move()
    {
        echo "I'm moving!" . '<br>';
        // TODO: Implement move() method.
    }

    public function changeSpeed($type)
    {
        if ($type > self::MAX_SPEED || $type < 0 ) {
            $this->currentSpeed = 0;
            echo "X3 doesn't have '" . $type . "' speed!!!" . '<br>';
            return;
        }
        $this->currentSpeed = $type;
        echo "I've changed the speed -  (" . $type . ")" . '<br>';
    }

    public function turnOn()
    {
        $this->status = true;
        echo "The car X3 was turned ON!" . '<br>';
    }

    public function turnOff()
    {
        $this->status = false;
        echo "The car X3 was turned OFF!" . '<br>';
    }

}