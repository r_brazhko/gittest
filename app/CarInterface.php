<?php

interface CarInterface
{
    public function move();

    public function turnOn();

    public function turnOff();

}

