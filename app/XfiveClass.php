<?php

class XfiveClass extends BmwAbstract implements CarInterface
{
    const MAX_SPEED = 4;

    public function __construct($owner)
    {
        $this->owner = $owner;
        $this->price = 15000;
        $this->doorCount = 2;
        $this->engine = 1.8;
    }

    public function move()
    {
        // TODO: Implement move() method.
    }

    public function changeSpeed($type)
    {
        if ($type > self::MAX_SPEED || $type < 0 ) {
            $this->currentSpeed = 0;
            return;
        }
        $this->currentSpeed = $type;
    }

    public function turnOn()
    {
        $this->status = true;
    }

    public function turnOff()
    {
        $this->status = false;
    }

}