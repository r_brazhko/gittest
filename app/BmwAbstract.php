<?php

abstract class BmwAbstract
{
    protected $owner;

    protected $price;

    protected $doorCount;

    protected $engine;

    protected $currentSpeed = 0;

    protected $status = false;

    abstract public function changeSpeed($type);

    public function getOwner()
    {
        return $this->owner;
    }


    public function getEngine()
    {
        return $this->engine;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDoorCount()
    {
        return $this->doorCount;
    }

    public function getCurrentSpeed()
    {
        return $this->currentSpeed;
    }


    public function showFullInfo()
    {
        echo "<br>---------------Car X3---------------<br>";
        echo 'Owner: ' . $this->getOwner() . '<br>';
        echo 'Price: ' . $this->getPrice() . '<br>';
        echo 'Engine: ' . $this->getEngine() . '<br>';
        echo 'Doors: ' . $this->getDoorCount() . '<br>';
    }


}
